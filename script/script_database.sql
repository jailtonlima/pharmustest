USE [master]
GO

/****** Object:  Database [Agenda]    Script Date: 23/07/2019 16:22:29 ******/
CREATE DATABASE [Agenda]
GO
CREATE TABLE [dbo].[Contatos](
	[Id] [int] IDENTITY(1,1) NOT NULL ,
	[Nome] [varchar](150) NOT NULL,
	[Telefone] [varchar](20) NOT NULL,
	[Email] [varchar](100) NOT NULL
) ON [PRIMARY]

GO

