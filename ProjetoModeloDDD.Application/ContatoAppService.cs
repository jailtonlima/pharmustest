﻿

using System.Collections.Generic;
using ProjetoModeloDDD.Application.Interface;
using ProjetoModeloDDD.Domain.Entities;
using ProjetoModeloDDD.Domain.Interfaces.Services;

namespace ProjetoModeloDDD.Application
{
    public class ContatoAppService : IContatoAppService
    {
        private readonly IContatoService _contatoService;

        public ContatoAppService(IContatoService contatoService)
        {
            _contatoService = contatoService;
        }

        public void Atualizar(Contato contato)
        {
            _contatoService.Update(contato);
        }

        public Contato GetById(int id)
        {
            return _contatoService.GetById(id);
        }

        public void Incluir(Contato contato)
        {
            _contatoService.Add(contato);
        }

        public IEnumerable<Contato> ListarTodos()
        {
            return _contatoService.GetAll();
        }
    }
}
