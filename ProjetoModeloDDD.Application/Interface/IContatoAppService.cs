﻿
using System.Collections.Generic;
using ProjetoModeloDDD.Domain.Entities;

namespace ProjetoModeloDDD.Application.Interface
{
    public interface IContatoAppService
    {
        void Incluir(Contato contato);
        Contato GetById(int id);
        IEnumerable<Contato> ListarTodos();
        void Atualizar(Contato contato);
    }
}
