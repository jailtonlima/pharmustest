﻿using AgendaClient.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgendaClient.ViewModel
{
    public class ContatoViewModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }

        public List<Contato> Contatos { get; set; }
    }
}