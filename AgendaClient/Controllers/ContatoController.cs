﻿using AgendaClient.Models;
using AgendaClient.ViewModel;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AgendaClient.Controllers
{
    public class ContatoController : Controller
    {


        private string UrlApi;
        public ContatoController()
        {
            this.UrlApi = ConfigurationManager.AppSettings["URLApi"];            
        }

            
        // GET: Contato
        public ActionResult Index()
        {
            var client = new RestClient(this.UrlApi);

            var request = new RestRequest("contato", Method.GET);
            var response = client.Execute<List<Contato>>(request);

            var contatos = new ContatoViewModel();
            
            contatos.Contatos = response.Data;

            return View(contatos);
        }

        // GET: Contato/Details/5
        public ActionResult Details(int id)
        {
            return View(new ContatoViewModel());
        }

        // GET: Contato/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Contato/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {

                var body = new Contato();
                body.Nome = collection[1];
                body.Telefone = collection[2];
                body.Email = collection[3];

                var client = new RestClient(this.UrlApi);

                var request = new RestRequest("contato", Method.POST);
                request.RequestFormat = DataFormat.Json;
                request.AddBody(body);

                var response = client.Execute(request);


                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Contato/Edit/5
        public ActionResult Edit(int id)
        {
            var client = new RestClient(this.UrlApi);

            var request = new RestRequest($"contato/{id}", Method.GET);
            var response = client.Execute<Contato>(request);

            var contatoVM = new ContatoViewModel();

            var contato = (Contato)response.Data;

            contatoVM.Nome = contato.Nome;
            contatoVM.Telefone = contato.Telefone;
            contatoVM.Email = contato.Email;

            return View(contatoVM);
        }

        // POST: Contato/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                var body = new Contato();
                body.Nome = collection[2];
                body.Telefone = collection[3];
                body.Email = collection[4];

                var client = new RestClient(this.UrlApi);

                var request = new RestRequest($"contato/{id}", Method.PUT);
                request.RequestFormat = DataFormat.Json;
                request.AddBody(body);

                var response = client.Execute(request);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Contato/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Contato/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
