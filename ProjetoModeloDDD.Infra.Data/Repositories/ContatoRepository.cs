﻿using Dapper;
using ProjetoModeloDDD.Domain.Entities;
using ProjetoModeloDDD.Domain.Interfaces.Repositories;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace ProjetoModeloDDD.Infra.Data.Repositories
{
    public class ContatoRepository : IContatoRepository
    {
        public void Add(Contato contato)
        {
            var parametros = new DynamicParameters();
            parametros.Add("Nome", contato.Nome, DbType.AnsiString);
            parametros.Add("Telefone", contato.Telefone, DbType.AnsiString);
            parametros.Add("Email", contato.Email, DbType.AnsiString);

            using (SqlConnection connection = SqlContext.GetConnection())
            {

                connection.Execute("INSERT INTO [dbo].[Contatos]([Nome],[Telefone],[Email]) Values(@Nome, @Telefone,  @Email)", parametros);
            }
        }

        public Contato GetById(int id)
        {

            string sql = $"SELECT  [Id],[Nome],[Telefone],[Email] FROM [Agenda].[dbo].[Contatos] WHERE Id = {id}";

            var retorno = new Contato();

            using (SqlConnection connection = SqlContext.GetConnection())
            {
                retorno = connection.Query<Contato>(sql).FirstOrDefault();
            }
            return retorno;

        }

        public IEnumerable<Contato> GetAll()
        {
            string sql = "SELECT * FROM Contatos";

            var retorno = new List<Contato>();

            using (SqlConnection connection = SqlContext.GetConnection())
            {
                retorno = connection.Query<Contato>(sql).ToList();
            }
            return retorno;
        }

        public void Update(Contato contato)
        {
            var parametros = new DynamicParameters();
            parametros.Add("Nome", contato.Nome, DbType.AnsiString);
            parametros.Add("Telefone", contato.Telefone, DbType.AnsiString);
            parametros.Add("Email", contato.Email, DbType.AnsiString);

            string updateQuery = $"UPDATE [Agenda].[dbo].[Contatos] SET Nome = @Nome, Telefone = @Telefone,  Email = @Email WHERE Id = {contato.Id}";
            using (SqlConnection connection = SqlContext.GetConnection())
            {
                var result = connection.Execute(updateQuery, new
                {
                    contato.Nome,
                    contato.Telefone,
                    contato.Email
                });
            }
        }
    }
}
