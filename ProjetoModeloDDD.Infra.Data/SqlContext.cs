﻿using System.Configuration;
using System.Data.SqlClient;

namespace ProjetoModeloDDD.Infra.Data
{
    public class SqlContext
    {
        private static SqlConnection _conn;

        public static SqlConnection GetConnection()
        {

            _conn = new SqlConnection(ConfigurationManager.ConnectionStrings["AgendaDBContext"].ConnectionString);
            _conn.Open();

            return _conn;
        }
    }
}
