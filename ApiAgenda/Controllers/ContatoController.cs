﻿using AutoMapper;
using ProjetoModeloDDD.Application.Interface;
using ProjetoModeloDDD.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ApiAgenda.Controllers
{
    public class ContatoController : ApiController
    {

        private readonly IContatoAppService _contatoAppService;

        public ContatoController(IContatoAppService contatoAppService)
        {
            _contatoAppService = contatoAppService;
        }
        // GET: api/Contato
        public IEnumerable<Contato> Get()
        {
            return _contatoAppService.ListarTodos();
        }

        // GET: api/Contato/5
        public Contato Get(int id)
        {
            return _contatoAppService.GetById(id);
        }

        // POST: api/Contato
        public void Post([FromBody]Contato contato)
        {
            _contatoAppService.Incluir(contato);
        }

        // PUT: api/Contato/5
        public void Put(int id, [FromBody]Contato contato)
        {
            contato.Id = id;
            _contatoAppService.Atualizar(contato);
        }

        
    }
}
