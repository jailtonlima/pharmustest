﻿
using System.Collections.Generic;
using ProjetoModeloDDD.Domain.Entities;

namespace ProjetoModeloDDD.Domain.Interfaces.Services
{
    public interface IContatoService
    {        
        void Add(Contato contato);
        Contato GetById(int id);
        IEnumerable<Contato> GetAll();
        void Update(Contato contato);
    }
}
