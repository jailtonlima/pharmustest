﻿using ProjetoModeloDDD.Domain.Entities;
using System.Collections.Generic;

namespace ProjetoModeloDDD.Domain.Interfaces.Repositories
{
    public interface IContatoRepository
    {
        void Add(Contato contato);
        Contato GetById(int id);
        IEnumerable<Contato> GetAll();
        void Update(Contato contato);
    }
}
