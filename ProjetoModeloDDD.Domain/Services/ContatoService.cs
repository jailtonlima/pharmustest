﻿

using System.Collections.Generic;
using System.Linq;
using ProjetoModeloDDD.Domain.Entities;
using ProjetoModeloDDD.Domain.Interfaces.Repositories;
using ProjetoModeloDDD.Domain.Interfaces.Services;

namespace ProjetoModeloDDD.Domain.Services
{
    public class ContatoService : IContatoService
    {
        private readonly IContatoRepository _contatoRepository;

        public ContatoService(IContatoRepository contatoRepository)
        {
            _contatoRepository = contatoRepository;
        }

        public void Add(Contato contato)
        {
            _contatoRepository.Add(contato);
        }

        public Contato GetById(int id)
        {
            return _contatoRepository.GetById(id);
        }

        public IEnumerable<Contato> GetAll()
        {
            return _contatoRepository.GetAll();
        }

        public void Update(Contato contato)
        {
            _contatoRepository.Update(contato);
        }
    }
}
